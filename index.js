const puppeteer = require('puppeteer');

(async () => {
    const browser = await puppeteer.launch({headless: false});
    const page = await browser.newPage();
    await page.goto('https://getbootstrap.com/docs/4.0/components/forms/');    

    var emailTextBox = await page.$("#inputEmail4");
    var passwordTextBox = await page.$("#inputPassword4");
    var addressTextBox = await page.$("#inputAddress");
    var address2TextBox = await page.$("#inputAddress2");
    var cityTextBox = await page.$("#inputCity");
    var stateDropdown = await page.$("#inputState");
    var zipTextBox = await page.$("#inputZip");
    var checkbox = await page.$("#gridCheck");
    var typeOptions = {delay: 50};

    await emailTextBox.type("robert.joseph.freund@gmail.com", typeOptions);
    await passwordTextBox.type("someSecretPassword", typeOptions);
    await addressTextBox.type("1234 Baker Street", typeOptions);
    await address2TextBox.type("Apt 3", typeOptions);
    await cityTextBox.type("Middleton", typeOptions);
    await stateDropdown.type(".", typeOptions);
    await zipTextBox.type("53562", typeOptions);
    await checkbox.press("Space", typeOptions);

    await page.keyboard.press("Tab");
    await page.keyboard.press("Space");

    await browser.close();
})();