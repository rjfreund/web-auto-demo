This project uses Google Puppeteer to automate web tasks.
https://github.com/GoogleChrome/puppeteer

- install Node.js from https://nodejs.org/en/
- clone this repository to directory using git (https://git-scm.com/)
- navigate to directory in terminal
- type to run:

```
npm install
node index.js
```
